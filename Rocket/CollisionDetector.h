#include <vector>
#include "Structs.h"
#include "BoundingVolume.h"
#include "AABB.h"
#include "OOBB.h"
#include "MinkowskySum.h"

class CollisionDetector
{
public:
	CollisionDetector();
	~CollisionDetector();
	std::vector<TrianglePair*>* detectCollisions(std::vector<Triangle*> *staticTriangles, std::vector<Triangle*> *dynamicTriangles);
	bool detectCollision(Triangle* a, Triangle* b);
private:
	
	BoundingVolume* boundingVolume;
	AABB* aABB;
	OOBB* oOBB;
	MinkowskySum* minkowskySum;
};
