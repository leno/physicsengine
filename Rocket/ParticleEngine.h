#pragma once
#include <vector>
#include "ForceGenerator.h"
#include "GravityGenerator.h"
#include "FrictionGenerator.h"
#include "DragGenerator.h"
#include "ParticleSystem.h"

class ParticleEngine
{
public:
	ParticleEngine();
	~ParticleEngine();
	void updateParticle(float deltaTime, ParticleSystem* particleSystem);
	void addForceGen(ForceGenerator* forceGenerator);
	void delForceGen(ForceGenerator* forceGenerator);
	void integrator(float deltaTime, ParticleSystem* particleSystem); // apply forces to particle // particle struct with M: force, mass, velocity, position

	ParticleSystem* particleSystem;

private:
	std::vector<ForceGenerator*> forceGenerators;
	Vertex* acceleration;
};