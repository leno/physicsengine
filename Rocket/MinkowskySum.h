#pragma once
#include "Structs.h"

class MinkowskySum
{
public:
	MinkowskySum();
	~MinkowskySum();

	bool minkowskySumDetection(Triangle* a, Triangle* b);
	static bool pnpoly(int nvert, float *vertx, float *verty, float testx, float testy);
};

