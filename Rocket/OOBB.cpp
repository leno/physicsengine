#include "OOBB.h"
#include <limits>
#include <algorithm>
#include <math.h>

OOBB::OOBB()
{
}


OOBB::~OOBB()
{
}

bool OOBB::oOBBDetection(Triangle* t1, Triangle* t2){
	OBB* a = t1->oOBB;
	OBB* b = t2->oOBB;

#if 1
		Vertex axis[4];

		Vertex edge;

		edge = a->point[2] - a->point[1];
		edge = Vertex(-edge.y, edge.x, 0.0f);
		edge.Normalize();
		axis[0] = Vertex(edge.x, edge.y,0.0f);

		edge = a->point[2] - a->point[3];
		edge = Vertex(-edge.y, edge.x, 0);
		edge.Normalize();
		axis[1] = Vertex(edge.x, edge.y,0.0f);

		edge = b->point[2] - b->point[1];
		edge = Vertex(-edge.y, edge.x, 0.0f);
		edge.Normalize();
		axis[2] = Vertex(edge.x, edge.y, 0.0f);

		edge = b->point[2] - b->point[3];
		edge = Vertex(-edge.y, edge.x, 0.0f);
		edge.Normalize();
		axis[3] = Vertex(edge.x, edge.y, 0.0f);

		for(int x = 0; x < 4; x++)
		{
			Vertex projected1 = OBB::project(*a, axis[x]);
			Vertex projected2 = OBB::project(*b, axis[x]);

			if(projected1.x > projected2.x && projected1.x < projected2.y)
			{
				continue;
			}

			if(projected1.y > projected2.x && projected1.y < projected2.y)
			{
				continue;
			}

			if(projected2.x > projected1.x && projected2.x < projected1.y)
			{
				continue;
			}

			if(projected2.y > projected1.x && projected2.y < projected1.y)
			{
				continue;
			}
			return false;
		}
		return true;

	}
#endif