#pragma once
#define DYNAMIC_COLLISION 0
#define COLLISION 0
#include <math.h>
#include <vector>


struct Vertex
{
	Vertex(){
		 x = 0.0f;
		 y = 0.0f;
		 z = 0.0f;
	}

	Vertex(const Vertex& vertex){
		 x = vertex.x;
		 y = vertex.y;
		 z = vertex.z;
	}

	Vertex(float x, float y, float z){
		this->x = x;
		this->y = y;
		this->z = z;
	}

	float x;
	float y;
	float z;

	const float operator[](int const& pos){
		if (pos == 0) return this->x;
		if (pos == 1) return this->y;
		if (pos == 2) return this->z;
	}

	//addition
	const Vertex operator+(Vertex const& rhs)
	{
		Vertex tmp = *this;
		tmp.x += rhs.x;  
		tmp.y += rhs.y;
		tmp.z += rhs.z;
		return tmp;
	}

	//Subtraction
	const Vertex operator-(Vertex const& rhs)
	{
		Vertex tmp= *this; 
		tmp.x -= rhs.x;  
		tmp.y -= rhs.y;
		tmp.z -= rhs.z;
		return tmp;
	}

	//Dotproduct
	const float operatorDot(Vertex const& rhs)
	{
		float tmp = 0.0f;
		tmp = this->x * rhs.x + this->y * rhs.y + this->z * rhs.z;
		return tmp;
	}

	static const float dot(Vertex const& lhs, Vertex const& rhs){
		float tmp = 0.0f;
		tmp = lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
		return tmp;
	}

	//Crossproduct
	const Vertex operatorCross(Vertex const& rhs)
	{
		Vertex tmp = *this;
		tmp.x = this->y * rhs.z - this->z * rhs.y;
		tmp.y = this->z * rhs.x - this->x * rhs.z;
		tmp.z = this->x * rhs.y - this->y * rhs.x;
		return tmp;
	}

	//Multiplikation
	const Vertex operator*(Vertex const& rhs){
		Vertex tmp = *this;
		tmp.x *= rhs.x;
		tmp.y *= rhs.y;
		tmp.z *= rhs.z;
		return tmp;
	}

	// skalar Multiplikation
	const Vertex operator*(float const& rhs)
	{
		Vertex tmp = *this;
		tmp.x *= rhs;
		tmp.y *= rhs;
		tmp.z *= rhs;
		return tmp;
	}

	// skalar Division
	const Vertex operator/(float const& rhs)
	{
		Vertex tmp = *this;
		tmp.x /= rhs;
		tmp.y /= rhs;
		tmp.z /= rhs;
		return tmp;
	}

	//Betrag
	const float amount(){
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}

	//Normalisieren
	const Vertex Normalize(void)
	{
		Vertex tmp = *this;
		float   length = this->amount(),
			len = 0;

		if (length == 0)
			return tmp;

		len = 1.0f / length;

		tmp.x *= len;
		tmp.y *= len;
		tmp.z *= len;
		
		return tmp;
	}

};
#if 1
struct OBB
{
	Vertex c;
	Vertex u[3];
	Vertex e;

	Vertex point[4];

	static Vertex project(OBB &obb, Vertex &axis)
	{
		float  min = Vertex::dot(axis, Vertex(obb.point[0].x, obb.point[0].y, 0.0f));
		float max = min;
		for (int i = 1; i < 4; i++)
		{
			// NOTE: the axis must be normalized to get accurate projections
			float p = Vertex::dot(axis, Vertex(obb.point[i].x, obb.point[i].y, 0.0f));
			if (p < min)
			{
				min = p;
			}
			else if (p > max)
			{
				max = p;
			}
		}
		Vertex proj = Vertex(min, max, 0.0f);
		return proj;
	}
	/** Updates the axes after the corners move.  Assumes the
	corners actually form a rectangle. */
	void computeAxes() {
		u[0] = point[1] - point[0];
		u[1] = point[3] - point[0];

		float length = u[0].amount(); // normalize
		u[0] = u[0] / (length * length);
		c.x = Vertex::dot(point[0], u[0]);

		length = u[1].amount(); // normalize
		u[1] = u[1] / (length * length);
		c.y = Vertex::dot(point[0], u[1]);
	}
};
#endif
struct Matrix33
{
	Vertex matrix[3];
};

struct Particle
{
	Particle(Vertex* position, Vertex* force, Vertex* velocity, float mass){
		this->position = position;
		this->force = force;
		this->velocity = velocity;
		this->mass = mass;
		enableCalculation = true;
	}

	Vertex* force;
	Vertex* position;
	Vertex* velocity;
	bool enableCalculation;
	float mass;
};

struct BoundingBox
{
	BoundingBox(){
	
	}

	BoundingBox(Vertex* a, Vertex* b, Vertex* c, Vertex* d){
		this->a = a;
		this->b = b;
		this->c = c;
		this->d = d;
	}

	Vertex* a;
	Vertex* b;
	Vertex* c;
	Vertex* d;

};


struct Triangle
{
	Triangle(){

	}

	Triangle(Vertex* a, Vertex* b, Vertex* c){
		particle = nullptr;
		dynamic = false;
		vertices[0] = a;
		vertices[1] = b;
		vertices[2] = c;

		center2D = new Vertex((vertices[0]->x + vertices[1]->x + vertices[2]->x) / 3, (vertices[0]->y + vertices[1]->y + vertices[2]->y) / 3, 0.0f);
		
		bVradius = calcBoundingVolumeRadius();
		aABB = calcAABB();
		oOBB = calcOOBB();
		sleeping = true;
	}

	Triangle(Vertex* a, Vertex* b, Vertex* c, bool dynamic){
		particle = nullptr;
		
		vertices[0] = a;
		vertices[1] = b;
		vertices[2] = c;

		center2D = new Vertex((vertices[0]->x + vertices[1]->x + vertices[2]->x) / 3, (vertices[0]->y + vertices[1]->y + vertices[2]->y) / 3, 0.0f);
		enableDynamic(dynamic);
		bVradius = calcBoundingVolumeRadius();
		aABB = calcAABB();
		oOBB = calcOOBB();
		sleeping = false;
	}

	
	Vertex* vertices[3];
	Vertex* center2D;
	float bVradius;
	Particle* particle;
	BoundingBox* aABB;
	OBB* oOBB;

	void enableDynamic(bool dynamic){
		this->dynamic = dynamic;
		if (dynamic && particle == nullptr){
			particle = createParticle(center2D, new Vertex(0.0f, 0.0f, 0.0f), new Vertex(0.0f, 0.0f, 0.0f), calcMass());
		}
		else if(!dynamic && particle != nullptr){
			particle = nullptr;
		}
	}

	bool isDynamic(){
		return dynamic;
	}

	float calcMass(){
		return 1.0f;
	}

	bool isSleeping(){
		return sleeping;
	}

	Particle* createParticle(Vertex* center, Vertex* force, Vertex* Velocity, float mass){
		return new Particle(center, force, Velocity, mass);
	}

	void enableSleepmode(bool enable){

		if (enable){
			particle->enableCalculation = false;
			sleeping = true;
		}
		else{
			particle->enableCalculation = true;
			sleeping = false;
		}
	}

	void updateTriangle(){
		Vertex diff = this->center2D->operator-(*particle->position);

		//Update Verteces
			for (int i = 0; i < 3; ++i){
				//vertices[i]->operator-(diff);
				vertices[i]->x -= diff.x;
				vertices[i]->y -= diff.y;
				vertices[i]->z -= diff.z;
			}

			// Update AABB
				aABB->a->x -= diff.x;
				aABB->a->y -= diff.y;
				aABB->a->z -= diff.z;

				aABB->b->x -= diff.x;
				aABB->b->y -= diff.y;
				aABB->b->z -= diff.z;

				aABB->c->x -= diff.x;
				aABB->c->y -= diff.y;
				aABB->c->z -= diff.z;

				aABB->d->x -= diff.x;
				aABB->d->y -= diff.y;
				aABB->d->z -= diff.z;

				//Update OOBB
				//center
				oOBB->c = oOBB->c - diff;

				//positions
				oOBB->point[0] = oOBB->point[0] - diff;
				oOBB->point[1] = oOBB->point[1] - diff;
				oOBB->point[2] = oOBB->point[2] - diff;
				oOBB->point[3] = oOBB->point[3] - diff;

				//Update Center2D
				this->center2D = new Vertex(particle->position->x, particle->position->y, 0.0f);
	}

private:
	bool dynamic;
	bool sleeping;
	OBB* calcOOBB(){
		OBB* tmp = new OBB();
		Vertex* hypo;
		Vertex* hightPoint;
		Vertex* anka;
		Vertex* geka;

		Vertex distAB = vertices[0]->operator-(*vertices[1]);
		Vertex distBC = vertices[1]->operator-(*vertices[2]);
		Vertex distAC = vertices[0]->operator-(*vertices[2]);
		if (distAB.amount() >= distBC.amount() && distAB.amount() > distAC.amount()){
			hypo = &distAB;
			anka = &distAC;
			geka = &distBC;
			tmp->point[0] = *vertices[0];
			tmp->point[1] = *vertices[1];
			hightPoint = vertices[2];
		}
		else if (distBC.amount() >= distAB.amount() && distBC.amount() > distAC.amount()){
			hypo = &distBC;
			anka = &distAB;
			geka = &distAC;
			tmp->point[0] = *vertices[1];
			tmp->point[1] = *vertices[2];
			hightPoint = vertices[0];
		}
		else{
			hypo = &distAC;
			anka = &distBC;
			geka = &distAB;
			tmp->point[0] = *vertices[2];
			tmp->point[1] = *vertices[0];
			hightPoint = vertices[1];
		}

		Vertex az(0.0f, 0.0f, 1.0f);
		Vertex normalizedHypo = hypo->Normalize();
		Vertex hypoNormale = normalizedHypo.operatorCross(az);

		float s = (hypo->amount() + anka->amount() + geka->amount()) * 0.5f;
		float height = (2 / hypo->amount()) * sqrt(s * (s - hypo->amount())* (s - anka->amount()) * (s - geka->amount()));
		
		Vertex hypoNormaleL = hypoNormale * height;
		Vertex center(hypo->amount() * 0.5f, height * 0.5f, 0.0f);
		Vertex e(hypo->amount() * 0.5f, height * 0.5f, 0.0f);

		
		tmp->c = center;
		tmp->e = e;
		tmp->point[2] = tmp->point[1] + hypoNormaleL;
		tmp->point[3] = tmp->point[0] + hypoNormaleL;
		tmp->computeAxes();


		return tmp;
	}

	float calcBoundingVolumeRadius(){
		
		Vertex ca = *center2D - *vertices[0];
		Vertex cb = *center2D - *vertices[1];
		Vertex cc = *center2D - *vertices[2];

		float caDist = sqrt(pow(ca.x, 2) + pow(ca.y, 2) + pow(ca.z, 2));
		float cbDist = sqrt(pow(cb.x, 2) + pow(cb.y, 2) + pow(cb.z, 2));
		float ccDist = sqrt(pow(cc.x, 2) + pow(cc.y, 2) + pow(cc.z, 2));
		if (caDist >= cbDist && caDist > ccDist) return caDist;
		if (cbDist >= caDist && cbDist > ccDist) return cbDist;
		return ccDist;
	}

	BoundingBox* calcAABB(){
		float maxX = vertices[0]->x;
		float maxY = vertices[0]->y;
		float minX = vertices[0]->x;
		float minY = vertices[0]->y;

		for (int i = 1; i < 3; ++i){
			if (vertices[i]->x > maxX){
				maxX = vertices[i]->x;
			}
			if (vertices[i]->y > maxY){
				maxY = vertices[i]->y;
			}
			if (vertices[i]->x < minX){
				minX = vertices[i]->x;
			}
			if (vertices[i]->y < minY){
				minY = vertices[i]->y;
			}
		}
		Vertex* upLeft = new Vertex(minX, minY, 0.0f);
		Vertex* upRight = new Vertex(maxX, minY, 0.0f);
		Vertex* downLeft = new Vertex(minX, maxY, 0.0f);
		Vertex* downRight = new Vertex(maxX, maxY, 0.0f);

		return new BoundingBox(downLeft, upLeft, upRight, downRight);
	}
};

struct TrianglePair
{
	TrianglePair(Triangle* tri1, Triangle* tri2){
		this->tri1 = tri1;
		this->tri2 = tri2;
	}
	Triangle* tri1;
	Triangle* tri2;
};

struct Rectangle
{
	Rectangle(Triangle* a, Triangle* b){
		this->a = a;
		this->b = b;

		center2D = new Vertex((a->center2D->x + b->center2D->x) / 2, (a->center2D->y + b->center2D->y) / 2, 0.0f);
	}
	Triangle* a;
	Triangle* b;
	Vertex* center2D;
	

	void updateRectangle(Vertex* center2D){

		a->updateTriangle();
		b->updateTriangle();

		this->center2D = center2D;
	}
};

