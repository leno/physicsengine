#include "CollisionDetector.h"
#include <vector>

CollisionDetector::CollisionDetector(){

	boundingVolume = new BoundingVolume();
	aABB = new AABB();
	oOBB = new OOBB();
	minkowskySum = new MinkowskySum();
}

CollisionDetector::~CollisionDetector(){

}

std::vector<TrianglePair*>* CollisionDetector::detectCollisions(std::vector<Triangle*> *staticTriangles, std::vector<Triangle*> *dynamicTriangles){
	std::vector<TrianglePair*>* trianglePairs = new std::vector<TrianglePair*>();
	// CD test against all dynamic triangles
	for (unsigned int i = 0; i < dynamicTriangles->size(); ++i){
#if DYNAMIC_COLLISION
		for (unsigned int j = 0; j < dynamicTriangles->size(); ++j){// und gegen alle anderen dynamischentriangles checken
			if (i != j){ // nicht gegen sich selbst testen
				if (detectCollision(dynamicTriangles->at(i), dynamicTriangles->at(j))){
					TrianglePair* triPair = new TrianglePair(dynamicTriangles->at(i), dynamicTriangles->at(j));
					trianglePairs->push_back(triPair);
				}
			}
		}
#endif
		// CD test against all static triangles
		for (unsigned int a = 0; a < staticTriangles->size(); ++a){
			if (detectCollision(dynamicTriangles->at(i), staticTriangles->at(a))){
				TrianglePair* triPair = new TrianglePair(dynamicTriangles->at(i), staticTriangles->at(a));
				trianglePairs->push_back(triPair);
			}
		}
	}
	return trianglePairs;
}

bool CollisionDetector::detectCollision(Triangle* a, Triangle* b){
	if (boundingVolume->boundingVolumeDetection(a,b)){
		if (aABB->aABBDetection(a,b)){
			if (oOBB->oOBBDetection(a,b)){
				if (minkowskySum->minkowskySumDetection(a,b)){
					return true;
				}
			}
		}
	}
	return false;
}