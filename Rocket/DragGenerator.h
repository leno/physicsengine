#pragma once
#include "ForceGenerator.h"

class DragGenerator : public ForceGenerator
{
public:
	DragGenerator();
	~DragGenerator();
	virtual void updateForce(ParticleSystem *particleSystem);
private:
	Vertex* drag;
};

