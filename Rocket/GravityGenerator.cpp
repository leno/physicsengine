#include "GravityGenerator.h"

GravityGenerator::GravityGenerator(float gX, float gY, float gZ)
{
	gravity = new Vertex(gX, gY, gZ);
}


GravityGenerator::~GravityGenerator()
{
}

void GravityGenerator::updateForce(ParticleSystem *particleSystem)
{
	for (unsigned int i = 0; i < particleSystem->particles.size(); i++)
	{
			particleSystem->particles.at(i)->force->x += gravity->x;
			particleSystem->particles.at(i)->force->y += gravity->y;
			particleSystem->particles.at(i)->force->z += gravity->z;
	}
}
