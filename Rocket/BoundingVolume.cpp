#include "BoundingVolume.h"


BoundingVolume::BoundingVolume()
{
}


BoundingVolume::~BoundingVolume()
{
}

bool BoundingVolume::boundingVolumeDetection(Triangle* a, Triangle* b){
	Vertex diff = a->center2D->operator-(*b->center2D);
	float dist = sqrt(pow(diff.x, 2) + pow(diff.y, 2) + pow(diff.z, 2));
	float radiusAddition = a->bVradius + b->bVradius;

	// whern the summed radii are greater then the distance thers a collosion
	if (dist < radiusAddition){
		return true;
	}
	return false;
}
