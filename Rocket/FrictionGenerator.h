#pragma once
#include "ForceGenerator.h"

class FrictionGenerator : public ForceGenerator
{
public:
	FrictionGenerator();
	~FrictionGenerator();
	virtual void updateForce(ParticleSystem *particleSystem);
private:
	Vertex* friction;
};

