#include <SFML\Graphics.hpp>

#include "Structs.h"
#include "ParticleEngine.h"
#include "CollisionDetector.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

//Triangles
std::vector<Triangle*> *staticTriangles = new std::vector<Triangle*>();
std::vector<Triangle*> *dynamicTriangles = new std::vector<Triangle*>();

//Shapes
std::vector<sf::Shape*> *dynamicShapes = new std::vector<sf::Shape*>();
std::vector<sf::Shape*> *staticShapes = new std::vector<sf::Shape*>();

//Create Particle System 
ParticleSystem* particleSystem = new ParticleSystem();

int findTriangleIndex(Triangle* tri){
	for (int i = 0; i < staticTriangles->size(); ++i){
		if (staticTriangles->at(i) == tri){
			return i;
		}
	}
	for (int i = 0; i < dynamicTriangles->size(); ++i){
		if (dynamicTriangles->at(i) == tri){
			return i;
		}
	}
}

int findParticleIndex(Particle* part){
	for (int i = 0; i < particleSystem->particles.size(); ++i){
		if (particleSystem->particles.at(i) == part){
			return i;
		}
	}
}

Triangle* createTriangle2D(float x, float y, float w, float h, bool dynamic){
	Vertex* v0 = new Vertex(x + w/2, y, 0.0f);
	Vertex* v1 = new Vertex(x - w/2, y, 0.0f);
	Vertex* v2 = new Vertex(x, y + h, 0.0f);
	Triangle* tri = new Triangle(v0, v1, v2, dynamic);

	sf::ConvexShape *triangle = new sf::ConvexShape();
	triangle->setPointCount(3);
	triangle->setPoint(0, sf::Vector2f(tri->vertices[0]->x, tri->vertices[0]->y));
	triangle->setPoint(1, sf::Vector2f(tri->vertices[1]->x, tri->vertices[1]->y));
	triangle->setPoint(2, sf::Vector2f(tri->vertices[2]->x, tri->vertices[2]->y));
	triangle->setOrigin(tri->center2D->x, tri->center2D->y);

	if (dynamic){
		dynamicTriangles->push_back(tri);
		particleSystem->particles.push_back(tri->particle);
		dynamicShapes->push_back(triangle);
	}
	else{
		staticTriangles->push_back(tri);
		staticShapes->push_back(triangle);
	}
	return tri;
}

void deleteTriangle(Triangle* tri){

	for (int i = 0; i < staticTriangles->size(); ++i){
		if (staticTriangles->at(i) == tri){
			staticShapes->erase(staticShapes->begin() + i);
			staticTriangles->erase(staticTriangles->begin() + i);
		}
	}

	for (int i = 0; i < dynamicTriangles->size(); ++i){
		if (dynamicTriangles->at(i) == tri){
			int index = findParticleIndex(tri->particle);
			particleSystem->particles.erase(particleSystem->particles.begin() + index);
			dynamicShapes->erase(dynamicShapes->begin() + i);
			dynamicTriangles->erase(dynamicTriangles->begin() + i);
		}
	}
}

int findShapeIndex(sf::Shape* Shape){
	for (int i = 0; i < staticShapes->size(); ++i){
		if (staticShapes->at(i) == Shape){
			return i;
		}
	}
	for (int i = 0; i < dynamicShapes->size(); ++i){
		if (dynamicShapes->at(i) == Shape){
			return i;
		}
	}
}

void switchDynamic(Triangle* tri, bool dynamic){
	
	if (!dynamic && tri->isDynamic()){ // static setzten 
		int triIndex = findTriangleIndex(tri);
		int particleIndex = findParticleIndex(tri->particle);

		//Delete particle
		particleSystem->particles.erase(particleSystem->particles.begin() + particleIndex);
		//triangle dynamic disablen
		dynamicTriangles->at(triIndex)->enableDynamic(false);
		//push triangle to static Triangle vector
		staticTriangles->push_back(dynamicTriangles->at(triIndex));
		//delete triangle from dynamic Triangle vector 
		dynamicTriangles->erase(dynamicTriangles->begin() + triIndex);
		//push triangleShapes to static Shapes vector
		staticShapes->push_back(dynamicShapes->at(triIndex));
		//delete triangleShapes from dynamischen Shapes vector
		dynamicShapes->erase(dynamicShapes->begin() + triIndex);
	}
	else if (dynamic && !tri->isDynamic()){ //dynamic replace
		int triIndex = findTriangleIndex(tri);

		//triangle dynamic enablen
		staticTriangles->at(triIndex)->enableDynamic(true);
		//add particle
		particleSystem->particles.push_back(staticTriangles->at(triIndex)->particle);
		//push triangle to dynamic triangle vector
		dynamicTriangles->push_back(staticTriangles->at(triIndex));
		//delete Triangle from Statischen triangle vector
		staticTriangles->erase(staticTriangles->begin() + triIndex);
		//push triangleShape to dynamic shapes vector
		dynamicShapes->push_back(staticShapes->at(triIndex - 2));
		//delete triangleShape from static shapes vector
		staticShapes->erase(staticShapes->begin() + (triIndex - 2));

	}
}

Vertex* calcBouncingDirection(Triangle* active, Triangle* passive){
	Vertex collisionEdge(0.0f, 0.0f, 0.0f);

	Vertex a = *active->center2D - *passive->vertices[0];
	Vertex b = *active->center2D - *passive->vertices[1];
	Vertex c = *active->center2D - *passive->vertices[2];

	if (a.amount() >= b.amount() && a.amount() > c.amount()){
		collisionEdge = (*passive->vertices[1] - *passive->vertices[2]);//bc
	}
	else if (b.amount() >= a.amount() && b.amount() > c.amount()){
		collisionEdge = (*passive->vertices[0] - *passive->vertices[2]);//ac
	}
	else{
		collisionEdge = (*passive->vertices[0] - *passive->vertices[1]);//ab
	}
	Vertex edgeNormal(-collisionEdge.y, collisionEdge.x, 0.0f);

	Vertex normalizedEdgeNormal = edgeNormal.Normalize();
	Vertex tmpVelocity = *active->particle->velocity;
	Vertex velocity = tmpVelocity * 0.8f;
	float counter = Vertex::dot(velocity, normalizedEdgeNormal) * 2;

	Vertex tmpReflection = velocity - normalizedEdgeNormal * counter;
	Vertex *reflection = new Vertex(tmpReflection);
	return reflection;
}


int main(){
	srand(static_cast <unsigned> (time(0)));
	bool explode = true;
	sf::RenderWindow window(sf::VideoMode(1000, 1000, 32), "Physics2D");
	window.setFramerateLimit(60); // Set a framrate limit to reduce the CPU load
	//window.setMouseCursorVisible(false); // Hide the cursor

	//CollisionDetection
	CollisionDetector collisionDetector;

	//GROUND
	//create Ground (calc)
	Vertex* r0 = new Vertex(0.0f, 595.0f, 0.0f);//-+
	Vertex* r1 = new Vertex(0.0f, 580.0f, 0.0f);//--
	Vertex* r2 = new Vertex(795.0f, 580.0f, 0.0f);//+-
	Vertex* r3 = new Vertex(795.0f, 595.0f, 0.0f);//++

	//create Rectangle
	Triangle* triA = new Triangle(r0, r1, r2);
	Triangle* triB = new Triangle(r2, r3, r0);
	Rectangle* groundRect = new Rectangle(triA, triB);
	staticTriangles->push_back(triA);
	staticTriangles->push_back(triB);

	//create Ground (Visual)
	sf::ConvexShape ground;
	ground.setPointCount(4);
	ground.setPoint(0, sf::Vector2f(groundRect->a->vertices[0]->x, groundRect->a->vertices[0]->y));
	ground.setPoint(1, sf::Vector2f(groundRect->a->vertices[1]->x, groundRect->a->vertices[1]->y));
	ground.setPoint(2, sf::Vector2f(groundRect->a->vertices[2]->x, groundRect->a->vertices[2]->y));
	ground.setPoint(3, sf::Vector2f(groundRect->b->vertices[1]->x, groundRect->b->vertices[1]->y));
	ground.setOrigin(groundRect->center2D->x / 800, groundRect->center2D->y / 600);
	//ground.setRotation(10.0f);

#if COLLISION
	//create Triangles (calc)
	Triangle* tri1 = createTriangle2D(200, 100, 50, 50, true);
	tri1->particle->velocity = new Vertex(10.5f, 0.0f, 0.0f);
	Triangle* tri2 = createTriangle2D(400, 100, 50, 50, true);
#else
	//create Triangles (calc)
	Triangle* tri1 = createTriangle2D(200, 570, 50, -40, false);
	Triangle* tri2 = createTriangle2D(400, 580, 200, -50, false);
#endif

	//create a particleEngine (calc)
	ParticleEngine *particleEngine = new ParticleEngine();

	//add a Gravity Force Generator (calc)
	ForceGenerator *gravityGen = new GravityGenerator(0.0f, 9.81, 0.0f);
	particleEngine->addForceGen(gravityGen);
	
	//DeltaTime
	sf::Clock deltaClock;
	//Game Loop
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			default:
				break;
			}

		}
#if COLLISION

#else
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
			switchDynamic(tri1, true);
			tri1->particle->velocity = new Vertex(4.0f, -30.0f, 0.0f);
		}

		if (tri1->center2D->y < 200.0f){
			Vertex pos = *tri1->center2D;
			deleteTriangle(tri1);
			if (explode){
				float expStrenght = 10.0f;
				for (int i = 0; i < 100; ++i){
					float x = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
					float y = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
					
					Triangle* tmpTri = createTriangle2D(pos.x, pos.y, 20, 20, true);
					tmpTri->particle->velocity = new Vertex(x * expStrenght, y * expStrenght, 0.0f);
				}
			}
			explode = false;
		}
#endif

		//DeltaTime
		sf::Time deltaTime = deltaClock.restart();

		//clear Window before drawing
		window.clear(sf::Color(100, 100, 200));

		//CollisionDetection
		std::vector<TrianglePair*>* collisionPairs = collisionDetector.detectCollisions(staticTriangles, dynamicTriangles);
		for (int i = 0; i < collisionPairs->size(); ++i){
			if (collisionPairs->at(i)->tri1->isDynamic()){
				for (int j = 0; j < particleSystem->particles.size(); ++j){
					if (particleSystem->particles.at(j) == collisionPairs->at(i)->tri1->particle){
#if COLLISION
						switchDynamic(collisionPairs->at(i)->tri1, false);
#else
						Vertex* bounceDir = calcBouncingDirection(collisionPairs->at(i)->tri1, collisionPairs->at(i)->tri2);
						if (bounceDir->amount() > 0.9f){
							collisionPairs->at(i)->tri1->particle->velocity = bounceDir;
						}
						else if (!collisionPairs->at(i)->tri1->isSleeping())
						{
							//Push obects out of each other
							while (collisionDetector.detectCollision(collisionPairs->at(i)->tri1, collisionPairs->at(i)->tri2))
								{
									collisionPairs->at(i)->tri1->particle->position->y -= 0.1f;
									collisionPairs->at(i)->tri1->updateTriangle();
								}
							collisionPairs->at(i)->tri1->enableSleepmode(true);
						}
					
							

#endif	
					}
				}
			}
			if (collisionPairs->at(i)->tri2->isDynamic()){
				for (int j = 0; j < particleSystem->particles.size(); ++j){
					if (particleSystem->particles.at(j) == collisionPairs->at(i)->tri2->particle){
#if COLLISION
						switchDynamic(collisionPairs->at(i)->tri2, false);
#else
						Vertex* bounceDir = calcBouncingDirection(collisionPairs->at(i)->tri2, collisionPairs->at(i)->tri1);
						if (bounceDir->amount() > 0.9f){
							collisionPairs->at(i)->tri2->particle->velocity = bounceDir;
						}
						else if (!collisionPairs->at(i)->tri2->isSleeping())
						{
							//Push obects out of each other
							while (collisionDetector.detectCollision(collisionPairs->at(i)->tri2, collisionPairs->at(i)->tri1))
								{
									collisionPairs->at(i)->tri2->particle->position->y -= 0.1f;
									collisionPairs->at(i)->tri2->updateTriangle();
								}
							collisionPairs->at(i)->tri2->enableSleepmode(true);
						}
#endif	
					}
				}
			}
		}

		//ParticleEngine
		//update Particle
		particleEngine->updateParticle(deltaTime.asSeconds() * 20, particleSystem);

		//update centre2D and triangle position (calc)
		for (int i = 0; i < dynamicTriangles->size(); ++i){
			dynamicTriangles->at(i)->updateTriangle();
		}

		//update and draw staticShapes + 2 wegen der ground plane
		for (int i = 0; i < staticShapes->size(); ++i){
			staticShapes->at(i)->setPosition(staticTriangles->at(i + 2)->center2D->x, staticTriangles->at(i + 2)->center2D->y);
			window.draw(*staticShapes->at(i));
		}

		//update and draw dynamicShapes
		for (int i = 0; i < dynamicShapes->size(); ++i){
			dynamicShapes->at(i)->setPosition(dynamicTriangles->at(i)->center2D->x, dynamicTriangles->at(i)->center2D->y);
			window.draw(*dynamicShapes->at(i));
		}

		// draw to screen
		window.draw(ground);
		// debug view
		#if COLLISION 
			//-------------------------------------------------------------------------------------------
			//Dynamics
			//-------------------------------------------------------------------------------------------

			//Triangle1

			sf::ConvexShape oOBB1;
			oOBB1.setPointCount(4);
			oOBB1.setPoint(0, sf::Vector2f(tri1->oOBB->point[0].x, tri1->oOBB->point[0].y));
			oOBB1.setPoint(1, sf::Vector2f(tri1->oOBB->point[1].x, tri1->oOBB->point[1].y));
			oOBB1.setPoint(2, sf::Vector2f(tri1->oOBB->point[2].x, tri1->oOBB->point[2].y));
			oOBB1.setPoint(3, sf::Vector2f(tri1->oOBB->point[3].x, tri1->oOBB->point[3].y));
			oOBB1.setFillColor(sf::Color::Transparent);
			oOBB1.setOutlineColor(sf::Color::Green);
			oOBB1.setOutlineThickness(1);
			
			sf::ConvexShape aABB1;
			aABB1.setPointCount(4);
			aABB1.setPoint(0, sf::Vector2f(tri1->aABB->a->x, tri1->aABB->a->y));
			aABB1.setPoint(1, sf::Vector2f(tri1->aABB->b->x, tri1->aABB->b->y));
			aABB1.setPoint(2, sf::Vector2f(tri1->aABB->c->x, tri1->aABB->c->y));
			aABB1.setPoint(3, sf::Vector2f(tri1->aABB->d->x, tri1->aABB->d->y));
			aABB1.setFillColor(sf::Color::Transparent);
			aABB1.setOutlineColor(sf::Color::Green);
			aABB1.setOutlineThickness(1);
			//aABB1.setOrigin(tri1->center2D->x, tri1->center2D->y);

			sf::CircleShape boundingVolume1(tri1->bVradius);
			boundingVolume1.setPosition(tri1->center2D->x - tri1->bVradius, tri1->center2D->y - tri1->bVradius);
			boundingVolume1.setFillColor(sf::Color::Transparent);
			boundingVolume1.setOutlineColor(sf::Color::Red);
			boundingVolume1.setOutlineThickness(1);

			sf::CircleShape circle1(2.0f);
			circle1.setPosition(tri1->center2D->x, tri1->center2D->y);
			circle1.setFillColor(sf::Color::Black);

			sf::CircleShape circle11(2.0f);
			circle11.setPosition(tri1->vertices[0]->x, tri1->vertices[0]->y);
			circle11.setFillColor(sf::Color::Black);

			sf::CircleShape circle12(2.0f);
			circle12.setPosition(tri1->vertices[1]->x, tri1->vertices[1]->y);
			circle12.setFillColor(sf::Color::Black);

			sf::CircleShape circle13(2.0f);
			circle13.setPosition(tri1->vertices[2]->x, tri1->vertices[2]->y);
			circle13.setFillColor(sf::Color::Black);

			//Triangle2

			sf::ConvexShape oOBB2;
			oOBB2.setPointCount(4);
			oOBB2.setPoint(0, sf::Vector2f(tri2->oOBB->point[0].x, tri2->oOBB->point[0].y));
			oOBB2.setPoint(1, sf::Vector2f(tri2->oOBB->point[1].x, tri2->oOBB->point[1].y));
			oOBB2.setPoint(2, sf::Vector2f(tri2->oOBB->point[2].x, tri2->oOBB->point[2].y));
			oOBB2.setPoint(3, sf::Vector2f(tri2->oOBB->point[3].x, tri2->oOBB->point[3].y));
			oOBB2.setFillColor(sf::Color::Transparent);
			oOBB2.setOutlineColor(sf::Color::Green);
			oOBB2.setOutlineThickness(1);

			sf::ConvexShape aABB2;
			aABB2.setPointCount(4);
			aABB2.setPoint(0, sf::Vector2f(tri2->aABB->a->x, tri2->aABB->a->y));
			aABB2.setPoint(1, sf::Vector2f(tri2->aABB->b->x, tri2->aABB->b->y));
			aABB2.setPoint(2, sf::Vector2f(tri2->aABB->c->x, tri2->aABB->c->y));
			aABB2.setPoint(3, sf::Vector2f(tri2->aABB->d->x, tri2->aABB->d->y));
			aABB2.setFillColor(sf::Color::Transparent);
			aABB2.setOutlineColor(sf::Color::Green);
			aABB2.setOutlineThickness(1);

			sf::CircleShape boundingVolume2(tri2->bVradius);
			boundingVolume2.setPosition(tri2->center2D->x - tri2->bVradius, tri2->center2D->y - tri2->bVradius);
			boundingVolume2.setFillColor(sf::Color::Transparent);
			boundingVolume2.setOutlineColor(sf::Color::Red);
			boundingVolume2.setOutlineThickness(1);

			sf::CircleShape circle2(2.0f);
			circle2.setPosition(tri2->center2D->x, tri2->center2D->y);
			circle2.setFillColor(sf::Color::Black);

			sf::CircleShape circle21(2.0f);
			circle21.setPosition(tri2->vertices[0]->x, tri2->vertices[0]->y);
			circle21.setFillColor(sf::Color::Black);

			sf::CircleShape circle22(2.0f);
			circle22.setPosition(tri2->vertices[1]->x, tri2->vertices[1]->y);
			circle22.setFillColor(sf::Color::Black);

			sf::CircleShape circle23(2.0f);
			circle23.setPosition(tri2->vertices[2]->x, tri2->vertices[2]->y);
			circle23.setFillColor(sf::Color::Black);

			//update Dynamics position(visual)
			circle1.setPosition(tri1->center2D->x, tri1->center2D->y);
			circle2.setPosition(tri2->center2D->x, tri2->center2D->y);


			//-------------------------------------------------------------------------------------------
			//Statics
			//-------------------------------------------------------------------------------------------

			//Rectangle
			sf::CircleShape rectangle0(2.0f);
			rectangle0.setPosition(groundRect->a->vertices[0]->x, groundRect->a->vertices[0]->y);
			rectangle0.setFillColor(sf::Color::Black);

			sf::CircleShape rectangle1(2.0f);
			rectangle1.setPosition(groundRect->a->vertices[1]->x, groundRect->a->vertices[1]->y);
			rectangle1.setFillColor(sf::Color::Black);

			sf::CircleShape rectangle2(2.0f);
			rectangle2.setPosition(groundRect->a->vertices[2]->x, groundRect->a->vertices[2]->y);
			rectangle2.setFillColor(sf::Color::Black);

			sf::CircleShape rectangle3(2.0f);
			rectangle3.setPosition(groundRect->b->vertices[1]->x, groundRect->b->vertices[1]->y);
			rectangle3.setFillColor(sf::Color::Black);

			sf::CircleShape rectangleCenter(2.0f);
			rectangleCenter.setPosition(groundRect->center2D->x, groundRect->center2D->y);
			rectangleCenter.setFillColor(sf::Color::Black);

			sf::CircleShape rectangleTri1Center(2.0f);
			rectangleTri1Center.setPosition(groundRect->a->center2D->x, groundRect->a->center2D->y);
			rectangleTri1Center.setFillColor(sf::Color::Black);

			sf::CircleShape rectangleTri2Center(2.0f);
			rectangleTri2Center.setPosition(groundRect->b->center2D->x, groundRect->b->center2D->y);
			rectangleTri2Center.setFillColor(sf::Color::Black);

			sf::CircleShape rectangleTriboundingVolume1(groundRect->a->bVradius);
			rectangleTriboundingVolume1.setPosition(groundRect->a->center2D->x - groundRect->a->bVradius, groundRect->a->center2D->y - groundRect->a->bVradius);
			rectangleTriboundingVolume1.setFillColor(sf::Color::Transparent);
			rectangleTriboundingVolume1.setOutlineColor(sf::Color::Red);
			rectangleTriboundingVolume1.setOutlineThickness(5);

			sf::CircleShape rectangleTriboundingVolume2(groundRect->b->bVradius);
			rectangleTriboundingVolume2.setPosition(groundRect->b->center2D->x - groundRect->a->bVradius, groundRect->b->center2D->y - groundRect->a->bVradius);
			rectangleTriboundingVolume2.setFillColor(sf::Color::Transparent);
			rectangleTriboundingVolume2.setOutlineColor(sf::Color::Red);
			rectangleTriboundingVolume2.setOutlineThickness(5);

			sf::ConvexShape aABB3;
			aABB3.setPointCount(4);
			aABB3.setPoint(0, sf::Vector2f(groundRect->a->aABB->a->x, groundRect->a->aABB->a->y));
			aABB3.setPoint(1, sf::Vector2f(groundRect->a->aABB->b->x, groundRect->a->aABB->b->y));
			aABB3.setPoint(2, sf::Vector2f(groundRect->a->aABB->c->x, groundRect->a->aABB->c->y));
			aABB3.setPoint(3, sf::Vector2f(groundRect->a->aABB->d->x, groundRect->a->aABB->d->y));
			aABB3.setFillColor(sf::Color::Transparent);
			aABB3.setOutlineColor(sf::Color::Green);
			aABB3.setOutlineThickness(1);

			sf::ConvexShape aABB4;
			aABB4.setPointCount(4);
			aABB4.setPoint(0, sf::Vector2f(groundRect->b->aABB->a->x, groundRect->b->aABB->a->y));
			aABB4.setPoint(1, sf::Vector2f(groundRect->b->aABB->b->x, groundRect->b->aABB->b->y));
			aABB4.setPoint(2, sf::Vector2f(groundRect->b->aABB->c->x, groundRect->b->aABB->c->y));
			aABB4.setPoint(3, sf::Vector2f(groundRect->b->aABB->d->x, groundRect->b->aABB->d->y));
			aABB4.setFillColor(sf::Color::Transparent);
			aABB4.setOutlineColor(sf::Color::Green);
			aABB4.setOutlineThickness(1);

			sf::ConvexShape oOBB3;
			oOBB3.setPointCount(4);
			oOBB3.setPoint(0, sf::Vector2f(groundRect->a->oOBB->point[0].x, groundRect->a->oOBB->point[0].y));
			oOBB3.setPoint(1, sf::Vector2f(groundRect->a->oOBB->point[1].x, groundRect->a->oOBB->point[1].y));
			oOBB3.setPoint(2, sf::Vector2f(groundRect->a->oOBB->point[2].x, groundRect->a->oOBB->point[2].y));
			oOBB3.setPoint(3, sf::Vector2f(groundRect->a->oOBB->point[3].x, groundRect->a->oOBB->point[3].y));
			oOBB3.setFillColor(sf::Color::Transparent);
			oOBB3.setOutlineColor(sf::Color::Green);
			oOBB3.setOutlineThickness(1);

			sf::ConvexShape oOBB4;
			oOBB4.setPointCount(4);
			oOBB4.setPoint(0, sf::Vector2f(groundRect->b->oOBB->point[0].x, groundRect->b->oOBB->point[0].y));
			oOBB4.setPoint(1, sf::Vector2f(groundRect->b->oOBB->point[1].x, groundRect->b->oOBB->point[1].y));
			oOBB4.setPoint(2, sf::Vector2f(groundRect->b->oOBB->point[2].x, groundRect->b->oOBB->point[2].y));
			oOBB4.setPoint(3, sf::Vector2f(groundRect->b->oOBB->point[3].x, groundRect->b->oOBB->point[3].y));
			oOBB4.setFillColor(sf::Color::Transparent);
			oOBB4.setOutlineColor(sf::Color::Green);
			oOBB4.setOutlineThickness(1);

			#if 1
				window.draw(circle1);
				window.draw(circle11);
				window.draw(circle12);
				window.draw(circle13);

				window.draw(circle2);
				window.draw(circle21);
				window.draw(circle22);
				window.draw(circle23);

				window.draw(rectangle0);
				window.draw(rectangle1);
				window.draw(rectangle2);
				window.draw(rectangle3);
				window.draw(rectangleCenter);
				window.draw(rectangleTri1Center);
				window.draw(rectangleTri2Center);

				window.draw(boundingVolume1);
				window.draw(boundingVolume2);

				window.draw(rectangleTriboundingVolume1);
				window.draw(rectangleTriboundingVolume2);

				window.draw(aABB1);
				window.draw(aABB2);

				window.draw(aABB3);
				window.draw(aABB4);
			#endif

				window.draw(oOBB2);
				window.draw(oOBB3);

				window.draw(oOBB1);
				window.draw(oOBB4);

		#endif	

		window.display();
		//system("PAUSE");
	}
	return 0;

}