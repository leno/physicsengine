#include "DragGenerator.h"

DragGenerator::DragGenerator()
{
}


DragGenerator::~DragGenerator()
{
}

void DragGenerator::updateForce(ParticleSystem *particleSystem)
{
	for (unsigned int i = 0; i < particleSystem->particles.size(); i++)
	{
		particleSystem->particles.at(i)->force->x += drag->x;
		particleSystem->particles.at(i)->force->y += drag->y;
		particleSystem->particles.at(i)->force->z += drag->z;
	}
}