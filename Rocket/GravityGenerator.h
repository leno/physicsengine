#pragma once
#include "ForceGenerator.h"

class GravityGenerator : public ForceGenerator
{
public:
	GravityGenerator(float gX, float gY, float gZ);
	~GravityGenerator();
	virtual void updateForce(ParticleSystem *particleSystem);
private:
	Vertex* gravity;
};

