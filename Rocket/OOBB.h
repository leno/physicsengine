#pragma once
#include "Structs.h"
class OOBB
{
public:
	OOBB();
	~OOBB();
	bool oOBBDetection(Triangle* a, Triangle* b);
	bool overlaps1Way(const OBB& other) const;
};

