#pragma once
#include "ParticleSystem.h"

class ForceGenerator
{
public:
	ForceGenerator(){}
	~ForceGenerator(){}
	virtual void updateForce(ParticleSystem *particleSystem) = 0;
};

