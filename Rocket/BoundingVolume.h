#pragma once
#include <vector>
#include "Structs.h"
class BoundingVolume
{
public:
	BoundingVolume();
	~BoundingVolume();
	bool boundingVolumeDetection(Triangle* a, Triangle* b);
};

