#include "MinkowskySum.h"
#include "QHull.h"

MinkowskySum::MinkowskySum()
{
}


MinkowskySum::~MinkowskySum()
{
}

bool MinkowskySum::minkowskySumDetection(Triangle* a, Triangle* b){

	std::vector<Point> minkoPoints;

	for (int i = 0; i < 3; ++i){
		for (int j = 0; j < 3; ++j){
			Vertex minkoDiff = *a->vertices[i] - *b->vertices[j];
			Point p;
			p.x = minkoDiff.x;
			p.y = minkoDiff.y;
			minkoPoints.push_back(p);
		}
	}

	std::vector<Point> points = convex_hull(minkoPoints);

	float* vertx = new float[points.size()];
	float* verty = new float[points.size()];
	for (int i = 0; i< points.size(); i++)
	{
		vertx[i] = points.at(i).x;
		verty[i] = points.at(i).y;
	}
	if (pnpoly(points.size(), vertx, verty, 0.0f, 0.0f) == true){
		return true;
	}
	return false;
}

bool MinkowskySum::pnpoly(int nvert, float *vertx, float *verty, float testx, float testy)
{
	// Copyright(c) 1970 - 2003, Wm.Randolph Franklin
	int i, j = 0;
	bool c = false;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((verty[i]>testy) != (verty[j]>testy)) &&
			(testx < (vertx[j] - vertx[i]) * (testy - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
			c = true;
	}
	return c;
}