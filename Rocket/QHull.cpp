﻿/*
- Find the points with minimum and maximum x coordinates, those are bound to be part of the convex.
- Use the line formed by the two points to divide the set in two subsets of points, which will be processed recursively.
- Determine the point, on one side of the line, with the maximum distance from the line. The two points found before along with this one form a triangle.
- The points lying inside of that triangle cannot be part of the convex hull and can therefore be ignored in the next steps.
- Repeat the previous two steps on the two lines formed by the triangle (not the initial line).
- Keep on doing so on until no more points are left, the recursion has come to an end and the points selected constitute the convex hull.
*/

#include "QHull.h"

#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <algorithm>
//#include "Timer.h"

using namespace std;

#define cross(p,a,b) \
 /* return crossproduct of vectors p-a and b-a */ \
 ((x[a]-x[p])*(y[b]-y[p]) - (y[a]-y[p])*(x[b]-x[p]))

#define leftturn(a,b,c) \
 /*true iff point c is lefthand of vector through points (a,b) */ \
 (cross(c,a,b)>0.0)

QHull::QHull()
	:x(), y(), belongs_to_hull(), mPt(), upper(NULL), lower(NULL),
	hullX(), hullY(),
	fullHullX(), fullHullY()
{
}

QHull::~QHull()
{
}

void QHull::clear()
{
	x.clear();
	y.clear();
	belongs_to_hull.clear();
	mPt.clear();
	hullX.clear();
	hullY.clear();
	upper = NULL;
	lower = NULL;
}

void QHull::addPoint(float x, float y)
{
	this->x.push_back(x);
	this->y.push_back(y);
}

std::vector<Vertex> QHull::getFullHull(){
	std::vector<Vertex> completeHull;
	for (int i = 0; i < fullHullX.size(); ++i){
		completeHull.push_back(Vertex(fullHullX.at(i), fullHullY.at(i), 0.0f));
	}
	return completeHull;
}

bool QHull::quickhull()
{
	if (x.size() != y.size()) {
		return false;
	}
	if (x.size() < 3) {
		return false;
	}
	unsigned int pointSize = x.size();
	belongs_to_hull.resize(pointSize + 100, 0);
	mPt.resize(pointSize + 100, 0);
	for (unsigned int i = 0; i < pointSize; i++) {
		belongs_to_hull[i] = 0;
		mPt[i] = i;
	}
	hullX.clear();
	hullY.clear();

	int minxpt = 0, maxxpt = 0;
	inithull(&mPt[0], pointSize, &minxpt, &maxxpt);

	int uppercnt = pointSize, lowercnt = pointSize;

	upper = delete_right(&mPt[0], &uppercnt, minxpt, maxxpt);
	lower = delete_right(&mPt[0], &lowercnt, maxxpt, minxpt);

	qh(upper, uppercnt);
	qh(lower, lowercnt);
	return true;
}

void QHull::storeFullHull()
{
	fullHullX.clear();
	fullHullY.clear();
	unsigned int imin = 1;
	float last_xmin = hullX[0];
	for (unsigned int i = 0; i < hullX.size(); i++) {
		float xmin = hullX[1];
		for (unsigned int ii = imin; ii < hullX.size(); ii++) {
			if (hullX[ii] > last_xmin && hullX[ii] < xmin) {
				xmin = hullX[ii];
				imin = ii;
			}
		}
		fullHullX.push_back(hullX[imin]);
		fullHullY.push_back(hullY[imin]);
	}
}


void QHull::store()
{
	//Um mit genau diesen Zufallszahlen testen zu k�nnen 
	//kann man diese hier in ein File schreiben
	ofstream outFile("sequence.txt");
	if (!outFile.is_open())
	{
		//cout << "Error creating file!\n";
		return;
	}
	for (unsigned int i = 0; i < x.size(); i++) {
		outFile << x[i] << "," << y[i] << endl;
	}

	outFile.close();
}

void QHull::inithull(int *pt, int n, int *minx, int *maxx)
{
	int p1, p2;
	int i;

	/* determine points p1,p2 with minimal and maximal x coordinate: */
	p1 = p2 = pt[0];   /* init. p1,p2 to first point */
#if 1
	for (i = 1; i<n; i++)   /* seq. search for minimum / maximum */
	{
		if (x[pt[i]] < x[p1])
			p1 = i;
		if (x[pt[i]] > x[p2])
			p2 = i;
	}
#else
	auto result = std::minmax(x.begin(), x.end());
	p1 = int(result.first); // min
	p2 = int(result.second); // max
#endif
	belongs_to_hull[p1] = 1;
	belongs_to_hull[p2] = 1;
	*minx = p1; *maxx = p2;
	hullX.push_back(x[p1]);
	hullY.push_back(y[p1]);
	hullX.push_back(x[p2]);
	hullY.push_back(y[p2]);
}

int *QHull::delete_right(int *pt, int *num, int p1, int p2)
{
	int leftcnt;
	int *left;
	int n = *num;

	/* delete all points in pt located right from line p1p2: */
	left = new int[n];
	left[0] = p1; left[1] = p2;
	leftcnt = 2;
	for (int j = 0; j<*num; j++)
	{
		if (!(pt[j] == p1 || pt[j] == p2))  /*p1 and p2 already in left[]*/
		{
			if (leftturn(p1, p2, pt[j])) /* point j is lefthand to vector p1p2 */
			{
				left[leftcnt++] = pt[j];
			}
		}
	}

	*num = leftcnt;
	return left;
}

void QHull::qh(int *pt, int n)
{
	/* DC step: select pivot point from pt */
	int pivot;
	int p1 = pt[0], p2 = pt[1];
	int *left1, *left2, leftcnt1, leftcnt2;

	/* DC step: select any pivot point from pt.
	We have p1==pt[0],p2==pt[1] */
	if (n == 2) return;

	if (n == 3) {
		/* one point (beyond old p1,p2) must belong to hull */
		belongs_to_hull[pt[2]] = 1;      /* saves a recursive call */
		hullX.push_back(x[pt[2]]);
		hullY.push_back(y[pt[2]]);
		return;
	}

	pivot = pivotize(pt, n);

	belongs_to_hull[pivot] = 1;
	hullX.push_back(x[pivot]);
	hullY.push_back(y[pivot]);
	leftcnt1 = n;

	left1 = delete_right(pt, &leftcnt1, p1, pivot);

	qh(left1, leftcnt1);
	leftcnt2 = n;

	left2 = delete_right(pt, &leftcnt2, pivot, p2);
	qh(left2, leftcnt2);
}

int QHull::pivotize(int *pt, int n)    /* n>=3 is assumed */
/* as pivot, select the point in pt[]-{p1,p2} with maximal
* cross_prod( pivot-p1, p2-p1 )
*/
{
	int i, p1 = pt[0], p2 = pt[1];
	int pivotpos = 2;
	float maxcross = cross(pt[2], p1, p2);
	for (i = 3; i<n; i++) {        /* sequential maximization */
		float newcross = cross(pt[i], p1, p2);
		if (newcross > maxcross) {
			maxcross = newcross;
			pivotpos = i;
		}
	}
	return pt[pivotpos];
}
