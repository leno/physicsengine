#include "ParticleEngine.h"


ParticleEngine::ParticleEngine()
{
}


ParticleEngine::~ParticleEngine()
{
}

void ParticleEngine::updateParticle(float deltaTime, ParticleSystem* particleSystem){

	// Sum forces of the single generators
	for (unsigned int i = 0; i < forceGenerators.size(); ++i){
		forceGenerators[i]->updateForce(particleSystem);
	}

	integrator(deltaTime, particleSystem);
}

void ParticleEngine::addForceGen(ForceGenerator* forceGen)
{
	forceGenerators.push_back(forceGen);
}

void ParticleEngine::delForceGen(ForceGenerator* forceGen)
{
	for (unsigned int i = 0; i < forceGenerators.size(); ++i){
		if (forceGenerators.at(i) == forceGen){
			forceGenerators.erase(forceGenerators.begin() + i);
		}
	}
}

// Apply forces to the particles // particle struct with M: force, mass, velocity, position
void ParticleEngine::integrator(float deltaTime, ParticleSystem* particleSystem){
	acceleration = new Vertex(0.0f, 0.0f, 0.0f);
	for (unsigned int i = 0; i < particleSystem->particles.size(); i++)
	{
		if (particleSystem->particles[i]->enableCalculation){
			// Calc acceleration 
			//acceleration[j] = (1 / particleSystem->particles[i]->mass) * particleSystem->particles[i]->force[j];
			acceleration->x = (particleSystem->particles[i]->mass / 10) * particleSystem->particles[i]->force->x;
			acceleration->y = (particleSystem->particles[i]->mass / 10) * particleSystem->particles[i]->force->y;
			acceleration->z = (particleSystem->particles[i]->mass / 10) * particleSystem->particles[i]->force->y;

			// calc new velocity
			particleSystem->particles[i]->velocity->x = particleSystem->particles[i]->velocity->x + acceleration->x * deltaTime;
			particleSystem->particles[i]->velocity->y = particleSystem->particles[i]->velocity->y + acceleration->y * deltaTime;
			particleSystem->particles[i]->velocity->z = particleSystem->particles[i]->velocity->z + acceleration->z * deltaTime;

			// calc new position
			particleSystem->particles[i]->position->x = particleSystem->particles[i]->position->x + particleSystem->particles[i]->velocity->x * deltaTime + acceleration->x * 0.5 * deltaTime * deltaTime;
			particleSystem->particles[i]->position->y = particleSystem->particles[i]->position->y + particleSystem->particles[i]->velocity->y * deltaTime + acceleration->y * 0.5 * deltaTime * deltaTime;
			particleSystem->particles[i]->position->z = particleSystem->particles[i]->position->z + particleSystem->particles[i]->velocity->z * deltaTime + acceleration->z * 0.5 * deltaTime * deltaTime;

			//reset forces to zero
			particleSystem->particles[i]->force->x = 0;
			particleSystem->particles[i]->force->y = 0;
			particleSystem->particles[i]->force->z = 0;
		}
	}
}