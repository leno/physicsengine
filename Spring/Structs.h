#ifndef STRUCT_H
#define STRUCT_H
#include <math.h>
#include <vector>

struct Vector2D
{
	Vector2D(){
		x = 0.0f;
		y = 0.0f;
	}

	Vector2D(const Vector2D& vertex){
		x = vertex.x;
		y = vertex.y;
	}

	Vector2D(float x, float y){
		this->x = x;
		this->y = y;
	}

	float x;
	float y;

	const float operator[](int const& pos){
		if (pos == 0) return this->x;
		if (pos == 1) return this->y;
	}

	//addition
	const Vector2D operator+(Vector2D const& rhs)
	{
		Vector2D tmp = *this;
		tmp.x += rhs.x;
		tmp.y += rhs.y;
		return tmp;
	}

	//Subtraction
	const Vector2D operator-(Vector2D const& rhs)
	{
		Vector2D tmp = *this;
		tmp.x -= rhs.x;
		tmp.y -= rhs.y;
		return tmp;
	}

	//Dotproduct
	const float operatorDot(Vector2D const& rhs)
	{
		float tmp = 0.0f;
		tmp = this->x * rhs.x + this->y * rhs.y;
		return tmp;
	}

	static const float dot(Vector2D const& lhs, Vector2D const& rhs){
		float tmp = 0.0f;
		tmp = lhs.x * rhs.x + lhs.y * rhs.y;
		return tmp;
	}

	/*//Crossproduct
	const Vector2 operatorCross(Vector2 const& rhs)
	{
		Vector2 tmp = *this;
		tmp.x = this->y * rhs.z - this->z * rhs.y;
		tmp.y = this->z * rhs.x - this->x * rhs.z;
		return tmp;
	}*/

	//Multiplikation
	const Vector2D operator*(Vector2D const& rhs){
		Vector2D tmp = *this;
		tmp.x *= rhs.x;
		tmp.y *= rhs.y;
		return tmp;
	}

	// skalar Multiplikation
	const Vector2D operator*(float const& rhs)
	{
		Vector2D tmp = *this;
		tmp.x *= rhs;
		tmp.y *= rhs;
		return tmp;
	}

	// skalar Division
	const Vector2D operator/(float const& rhs)
	{
		Vector2D tmp = *this;
		tmp.x /= rhs;
		tmp.y /= rhs;
		return tmp;
	}

	//Betrag
	const float amount(){
		return sqrt(pow(x, 2) + pow(y, 2));
	}

	//Normalisieren
	const Vector2D Normalize(void)
	{
		Vector2D tmp = *this;
		float   length = this->amount(),
			len = 0;

		if (length == 0)
			return tmp;

		len = 1.0f / length;

		tmp.x *= len;
		tmp.y *= len;

		return tmp;
	}

};

struct Particle
{
	Particle(Vector2D position, Vector2D force, Vector2D velocity, float mass, bool dynamic){
		this->position = position;
		this->force = force;
		this->velocity = velocity;
		this->mass = mass;
		this->dynamic = dynamic;
	}

	bool dynamic;
	Vector2D position;
	float mass;
	Vector2D force;
	Vector2D velocity;

	std::vector<Particle*> neighbors;
};
#endif