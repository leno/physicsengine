#include "SpringGenerator.h"


SpringGenerator::SpringGenerator(float l0, float k)
{
	this->l0 = l0;
	this->k = k;
}

SpringGenerator::~SpringGenerator()
{
}

void SpringGenerator::updateForce(ParticleSystem *particleSystem)
{
	for (unsigned int i = 0; i < particleSystem->particles.size(); i++)
	{
		for (unsigned int j = 0; j < particleSystem->particles.at(i)->neighbors.size(); j++){
			if (particleSystem->particles.at(i)->dynamic){
				Vector2D d = particleSystem->particles.at(i)->position - particleSystem->particles.at(i)->neighbors.at(j)->position;
				Vector2D dnormal = d.Normalize();
				float dLengh = d.amount();
				particleSystem->particles.at(i)->force = particleSystem->particles.at(i)->force + (dnormal * (-k * (dLengh - l0)));
			}
		}
	}
}