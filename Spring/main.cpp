#include "Structs.h"
#include <SFML\Graphics.hpp>
#include <SFML\System.hpp>
#include "ParticleEngine.h"
#include <iostream>
#define PARTICLE_DIST 60.0f
#define OFFSET_X 200.0f
#define OFFSET_Y 200.0f
std::vector<sf::Shape*> *shapes = new std::vector<sf::Shape*>();

//Create Particle System 
ParticleSystem* particleSystem = new ParticleSystem();

void createParticle(float posX, float posY, bool dynamic){

	sf::CircleShape* point = new sf::CircleShape(2.0f);
	point->setPosition(posX, posY);
	point->setFillColor(sf::Color::Black);
	shapes->push_back(point);

	Particle *particle = new Particle(Vector2D(posX, posY), Vector2D(0.0f, 0.0f), Vector2D(0.0f, 0.0f), 1.0f, dynamic);
	particleSystem->particles.push_back(particle);
}

int main(){
	srand(static_cast <unsigned> (time(0)));
	bool explode = true;
	sf::RenderWindow window(sf::VideoMode(1000, 1000, 32), "Physics2D");
	window.setFramerateLimit(60); // Set a framrate limit

	

	// Create Mesh
	for (int i = 0; i < 3; ++i){
		for (int j = 0; j < 3; ++j){
			if (j == 0){
				createParticle(OFFSET_X + i * PARTICLE_DIST, OFFSET_Y + j * PARTICLE_DIST, false);
			}
			else{
				createParticle(OFFSET_X + i * PARTICLE_DIST, OFFSET_Y + j * PARTICLE_DIST, true);
			}
		}
	}

	//find Neighbours
	//erste spalte
	particleSystem->particles.at(0)->neighbors.push_back(particleSystem->particles.at(1));
	particleSystem->particles.at(0)->neighbors.push_back(particleSystem->particles.at(3));

	particleSystem->particles.at(1)->neighbors.push_back(particleSystem->particles.at(0));
	particleSystem->particles.at(1)->neighbors.push_back(particleSystem->particles.at(4));
	particleSystem->particles.at(1)->neighbors.push_back(particleSystem->particles.at(2));

	particleSystem->particles.at(2)->neighbors.push_back(particleSystem->particles.at(1));
	particleSystem->particles.at(2)->neighbors.push_back(particleSystem->particles.at(5));
	
	//zweite spalte
	particleSystem->particles.at(3)->neighbors.push_back(particleSystem->particles.at(1));
	particleSystem->particles.at(3)->neighbors.push_back(particleSystem->particles.at(4));
	particleSystem->particles.at(3)->neighbors.push_back(particleSystem->particles.at(6));

	particleSystem->particles.at(4)->neighbors.push_back(particleSystem->particles.at(1));
	particleSystem->particles.at(4)->neighbors.push_back(particleSystem->particles.at(3));
	particleSystem->particles.at(4)->neighbors.push_back(particleSystem->particles.at(5));
	particleSystem->particles.at(4)->neighbors.push_back(particleSystem->particles.at(7));

	particleSystem->particles.at(5)->neighbors.push_back(particleSystem->particles.at(2));
	particleSystem->particles.at(5)->neighbors.push_back(particleSystem->particles.at(4));
	particleSystem->particles.at(5)->neighbors.push_back(particleSystem->particles.at(8));

	//dritte Spalte
	particleSystem->particles.at(6)->neighbors.push_back(particleSystem->particles.at(3));
	particleSystem->particles.at(6)->neighbors.push_back(particleSystem->particles.at(7));

	particleSystem->particles.at(7)->neighbors.push_back(particleSystem->particles.at(6));
	particleSystem->particles.at(7)->neighbors.push_back(particleSystem->particles.at(4));
	particleSystem->particles.at(7)->neighbors.push_back(particleSystem->particles.at(8));

	particleSystem->particles.at(8)->neighbors.push_back(particleSystem->particles.at(7));
	particleSystem->particles.at(8)->neighbors.push_back(particleSystem->particles.at(5));


	//create a particleEngine (calc)
	ParticleEngine *particleEngine = new ParticleEngine();

	//add a Gravity Force Generator 
	ForceGenerator *gravityGen = new GravityGenerator(0.0f, 9.81);
	particleEngine->addForceGen(gravityGen);

	//add a Spring Force Generator
	ForceGenerator *springGen = new SpringGenerator(PARTICLE_DIST, 1.0f);
	particleEngine->addForceGen(springGen);

	//DeltaTime
	sf::Clock deltaClock;

	

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			default:
				break;
			}

		}

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)){
			Vector2D mousePos(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
			for (int i = 0; i < particleSystem->particles.size(); ++i){
				if (particleSystem->particles.at(i)->dynamic){
					Vector2D dir = particleSystem->particles.at(i)->position - mousePos;
					float dist = dir.amount();
					if (dist <= 10.0f){
						particleSystem->particles.at(i)->position = mousePos;
					}
				}
			}

		}

		sf::Time deltaTime = deltaClock.restart();

		window.clear(sf::Color(100, 100, 200));

		//update Particle
		particleEngine->updateParticle(deltaTime.asSeconds() * 20, particleSystem);

		for (int i = 0; i < shapes->size(); ++i){
			shapes->at(i)->setPosition(particleSystem->particles.at(i)->position.x, particleSystem->particles.at(i)->position.y);

			window.draw(*shapes->at(i));
		}
		sf::Vertex line[] =
		{
			sf::Vertex(shapes->at(0)->getPosition()),
			sf::Vertex(shapes->at(1)->getPosition()),
			sf::Vertex(shapes->at(1)->getPosition()),
			sf::Vertex(shapes->at(2)->getPosition()),
			sf::Vertex(shapes->at(2)->getPosition()),
			sf::Vertex(shapes->at(5)->getPosition()),
			sf::Vertex(shapes->at(5)->getPosition()),
			sf::Vertex(shapes->at(8)->getPosition()),
			sf::Vertex(shapes->at(8)->getPosition()),
			sf::Vertex(shapes->at(7)->getPosition()),
			sf::Vertex(shapes->at(7)->getPosition()),
			sf::Vertex(shapes->at(6)->getPosition()),
			sf::Vertex(shapes->at(6)->getPosition()),
			sf::Vertex(shapes->at(3)->getPosition()),
			sf::Vertex(shapes->at(3)->getPosition()),
			sf::Vertex(shapes->at(0)->getPosition()),
			sf::Vertex(shapes->at(3)->getPosition()),
			sf::Vertex(shapes->at(4)->getPosition()),
			sf::Vertex(shapes->at(4)->getPosition()),
			sf::Vertex(shapes->at(1)->getPosition()),
			sf::Vertex(shapes->at(4)->getPosition()),
			sf::Vertex(shapes->at(5)->getPosition()),
			sf::Vertex(shapes->at(4)->getPosition()),
			sf::Vertex(shapes->at(7)->getPosition())
		};
		window.draw(line, 24, sf::Lines);
		window.display();
		//system("PAUSE");
	}
	return 0;

}