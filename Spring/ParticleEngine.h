#ifndef PARTICLEENGINE_H
#define PARTICLEENGINE_H

#include <vector>
#include "ForceGenerator.h"
#include "GravityGenerator.h"
#include "SpringGenerator.h"
#include "ParticleSystem.h"

class ParticleEngine
{
public:
	ParticleEngine();
	~ParticleEngine();
	void updateParticle(float deltaTime, ParticleSystem* particleSystem);
	void addForceGen(ForceGenerator* forceGenerator);
	void delForceGen(ForceGenerator* forceGenerator);
	void integrator(float deltaTime, ParticleSystem* particleSystem); // add force to partcles // particle struct mit M: force, mass, velocity, position

	ParticleSystem* particleSystem;

private:
	std::vector<ForceGenerator*> forceGenerators;
	Vector2D* acceleration;
};

#endif