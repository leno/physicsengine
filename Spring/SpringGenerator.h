#ifndef SPRINGGENERATOR_H
#define SPRINGGENERATOR_H
#include "ForceGenerator.h"

class SpringGenerator : public ForceGenerator
{
public:
	SpringGenerator(float l0, float k);
	~SpringGenerator();
	virtual void updateForce(ParticleSystem *particleSystem);
private:
	float l0;
	float k;
};
#endif
