#ifndef GRAVITYGENERATOR_H
#define GRAVITYGENERATOR_H
#include "ForceGenerator.h"

class GravityGenerator : public ForceGenerator
{
public:
	GravityGenerator(float gX, float gY);
	~GravityGenerator();
	virtual void updateForce(ParticleSystem *particleSystem);
private:
	Vector2D* gravity;
};

#endif

