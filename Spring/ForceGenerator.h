#ifndef FORCEGENERATOR_H
#define FORCEGENERATOR_H
#include "ParticleSystem.h"

class ForceGenerator
{
public:
	ForceGenerator(){}
	~ForceGenerator(){}
	virtual void updateForce(ParticleSystem *particleSystem) = 0;
};
#endif

