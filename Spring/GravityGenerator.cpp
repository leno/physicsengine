#include "GravityGenerator.h"

GravityGenerator::GravityGenerator(float gX, float gY)
{
	gravity = new Vector2D(gX, gY);
}


GravityGenerator::~GravityGenerator()
{
}

void GravityGenerator::updateForce(ParticleSystem *particleSystem)
{
	for (unsigned int i = 0; i < particleSystem->particles.size(); i++)
	{
		if (particleSystem->particles.at(i)->dynamic){
			particleSystem->particles.at(i)->force.x += gravity->x;
			particleSystem->particles.at(i)->force.y += gravity->y;
		}
	}
}
